# Cross Site Scripting Story


## Story Outline

This article is focused on providing clear, simple, actionable guidance for preventing Cross Site Scripting flaws in your applications.


## Story Organization
**Story Branch**: master
> `git checkout master`

**Practical task tag for self-study**: task
> `git checkout task`

Tags: #flastapi #python #sql #cross_site_scripting
